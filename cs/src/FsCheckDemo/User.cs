using System;

namespace FsCheckDemo {
    public class User {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string MailingAddress { get; set; }
        public decimal PointsBalance { get; set; }
        
        public override string ToString() => $"{{User: {UserId} Em:{Email} N:{Name} A:{MailingAddress} P:{PointsBalance}}}";
                
        public override bool Equals(Object obj) 
        {
            var otherUser = obj as User;
            if (otherUser == null) { return false; }
            else {
                return UserId == otherUser.UserId
                    && Email == otherUser.Email
                    && Name == otherUser.Name
                    && MailingAddress == otherUser.MailingAddress
                    && PointsBalance == otherUser.PointsBalance;
             }
        }
        
        public override int GetHashCode() 
        {
             return UserId ^ Email.GetHashCode()
                ^ Name.GetHashCode()
                ^ MailingAddress.GetHashCode()
                ^ PointsBalance.GetHashCode();
        }       
        
    }
}