package au.com.codesplice.portablesupernaturalanimals

fun String.normaliseEmail(): String {
  return this.toLowerCase()
}

fun <T> MutableList<T>.copy(): MutableList<T> {
  val list: MutableList<T> = mutableListOf()
  list.addAll(this)
  return list
}

fun <T> List<T>.toMutableList(): MutableList<T> {
  val list: MutableList<T> = mutableListOf()
  list.addAll(this)
  return list
}