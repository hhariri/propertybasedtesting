using System;
using System.Collections.Generic;
using System.Linq;

namespace FsCheckDemo {
    public class UserController {
        private ICollection<User> userDb;
        
        public UserController(ICollection<User> userDb) {
            this.userDb = userDb;
        }
        
        public void Create(UserDTO userData) {
            var user = userData.ToUser();
            user.UserId = userDb.Select(u => u.UserId).DefaultIfEmpty(0).Max() + 1;            
            user.Email = user.Email.NormaliseEmail();
            userDb.Add(user);
        }
        
        public UserDTO Get(string email) {
            return userDb.Where(u => u.Email == email)
                .Select(u => new UserDTO(u))
                .SingleOrDefault();
        }
        
        public bool TransferPoints(int fromUserId, int toUserId, decimal amount) {
            var fromUser = userDb.SingleOrDefault(u => u.UserId == fromUserId);
            var toUser = userDb.SingleOrDefault(u => u.UserId == toUserId);
            if (fromUser == null || toUser == null) {
                return false;
            }
            if (fromUser.PointsBalance < amount) {
                return false;
            }
            fromUser.PointsBalance -= amount;
            toUser.PointsBalance += amount;
            return true;
        }
    }

    public static class StringExtensions {
        public static string NormaliseEmail(this string email) {
            return email.ToLower();
        }
    }
}