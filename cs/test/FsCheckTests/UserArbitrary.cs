using System;
using System.Collections.Generic;
using System.Linq;
using FsCheckDemo;
using FsCheck;

namespace FsCheckTests {
    
    public class UserArbitrary {
        
        private static Gen<decimal> Balances() {
            return Gen.Choose(0, int.MaxValue).Select(i => ((decimal)i)/100m);
        }
        
        public static Arbitrary<UserDTO> UserDTOs() {
            return Arb.From(
                from i in Gen.Choose(1, int.MaxValue)
                from n in Arb.Generate<string>()
                from e in Arb.Generate<string>().Where(s => !string.IsNullOrEmpty(s))
                from m in Arb.Generate<string>()
                from b in Balances()
                select new UserDTO {
                    UserId = i, 
                    Name = n, 
                    Email = e, 
                    MailingAddress = m, 
                    PointsBalance = b
                }
            );
        }
        
        public static Arbitrary<User> Users() {
            return Arb.From(
                from i in Gen.Choose(1, int.MaxValue)
                from n in Arb.Generate<string>()
                from e in Arb.Generate<string>().Where(s => !string.IsNullOrEmpty(s))
                from m in Arb.Generate<string>()
                from b in Balances()
                select new User {
                    UserId = i, 
                    Name = n, 
                    Email = e, 
                    MailingAddress = m, 
                    PointsBalance = b
                }
            );
        }
        
        public static Arbitrary<List<User>> DB() {
            return Arb.From(Gen.ArrayOf<User>(Users().Generator)
                    .Select(a => a.ToList())
            );
        }
    }
}