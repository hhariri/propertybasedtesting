//
//  Widget.swift
//  SwiftCheckDemo
//
//  Created by Sam Ritchie on 2/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import Foundation

public enum WidgetStatus: Int { case good = 0, bad, ugly }

public struct Widget {
    public let value: Int
    public let status: WidgetStatus
    
    public init(value: Int, status: WidgetStatus) {
        self.value = value
        self.status = status
    }
}

public struct Doodad {
    public let isTransmogrified: Bool
    public let wimpleGlumpf: String
    
    public init(isTransmogrified: Bool, wimpleGlumpf: String) {
        self.isTransmogrified = isTransmogrified
        self.wimpleGlumpf = wimpleGlumpf
    }
}

public struct Thingamajig {
    public let frumpShizzle: String
    
    public init(frumpShizzle: String) {
        self.frumpShizzle = frumpShizzle
    }
}

public class WidgetProcessor {
    private var firstThingThatShouldNeverBeFalse = true
    private var secondThingThatShouldNeverBeFalse = true
    private var thirdThingThatShouldNeverBeFalse = true
    
    public var sum = 0
    public var count = 0
    
    public init() {}
    
    public func processWidget(_ widget: Widget) {
        guard (firstThingThatShouldNeverBeFalse || secondThingThatShouldNeverBeFalse || thirdThingThatShouldNeverBeFalse)
            else { return }
        
        sum += widget.value
        count += 1
        
        if widget.status == .ugly && !secondThingThatShouldNeverBeFalse {
            thirdThingThatShouldNeverBeFalse = false;
        }
    }
    
    public func swizzleDoodad(_ doodad: Doodad) {
        if doodad.isTransmogrified && doodad.wimpleGlumpf.characters.isEmpty && !firstThingThatShouldNeverBeFalse {
            secondThingThatShouldNeverBeFalse = false
        }
    }
    
    public func donkifyThingamajig(_ thingamajig: Thingamajig) {
        if thingamajig.frumpShizzle.contains("Z") {
            firstThingThatShouldNeverBeFalse = false
        }
    }
}
