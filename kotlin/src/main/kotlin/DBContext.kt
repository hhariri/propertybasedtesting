package au.com.codesplice.portablesupernaturalanimals

class DBContext(val monsters: MutableList<Monster> = mutableListOf(), val friends: MutableList<Friend> = mutableListOf(), var coinBalance: Int = 0) {
  fun collect(new: Monster) {
    val new = new.copy()
    monsters.add(new)
  }

  fun addFriend(friend: Friend) {
    val friend = friend.copy(email = friend.email.normaliseEmail())
    if (!isFriend(friend.email)) {
      friends.add(friend)
    }
  }

  fun isFriend(email: String): Boolean {
    return friends.find { it.email == email.normaliseEmail() } != null
  }

  fun get(id: String): Monster? {
    return monsters.find { it.id == id.toLowerCase() }
  }

  fun moveToGym(gym: Gym, id: String) {
    val monster = get(id)
    if (monster !== null && !monster.isFainted()) {
      gym.addMonster(monster)
      monsters.remove(monster)
    }
  } 

  fun purchase(item: StoreItem, quantity: Int) {
    if (item.price * quantity <= this.coinBalance) {
      this.coinBalance -= (item.price * quantity)
    } 
  }

  override fun toString(): String {
    return "DBContext(\nmonsters: [${monsters.map { it.toString() }.joinToString("\n")}]\n" + 
      "friends: [${friends.map { it.toString() }.joinToString("\n")}]\n" + 
      "coinBalance: $coinBalance\n)"
  }

  fun copy(): DBContext {
    return DBContext(monsters.copy(), friends.copy(), coinBalance)
  }

  override fun equals(other: Any?): Boolean {
    val otherCtx = other as? DBContext 
    if (otherCtx === null) {
      return false
    } else {
      return monsters == otherCtx?.monsters && friends == otherCtx?.friends && coinBalance == otherCtx?.coinBalance
    }
  }
}
