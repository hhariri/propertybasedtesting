namespace FsCheckDemo {

    public class UserDTO {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string MailingAddress { get; set; }
        public decimal PointsBalance { get; set; }
        
        public UserDTO() {}
        public UserDTO(User user) {
            UserId = user.UserId;
            Email = user.Email;
            Name = user.Name;
            PointsBalance = user.PointsBalance;
        }
        
        public User ToUser() {
            return new User {
                UserId = this.UserId,
                Email = this.Email,
                Name = this.Name,
                MailingAddress = this.MailingAddress,
                PointsBalance = this.PointsBalance
            };
        }
        
        public override string ToString() => $"{{User: {UserId} Em:{Email} N:{Name} A:{MailingAddress} P:{PointsBalance}}}";
    }
}