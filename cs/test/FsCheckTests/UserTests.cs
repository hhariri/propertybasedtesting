using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using FsCheckDemo;

namespace FsCheckTests {
 
    public class UserControllerTests {
        private List<User> mockDb;
        private UserController controller;
        
        public UserControllerTests() {
            mockDb = new List<User> { 
                new User {UserId = 1, Name = "Roger", Email = "roger@example.com", PointsBalance = 50 },
                new User {UserId = 2, Name = "Jennifer", Email = "jen@example.com", PointsBalance = 70 },
                new User {UserId = 3, Name = "Terrance", Email = "terry@example.com", PointsBalance = 30 },
                new User {UserId = 4, Name = "Susan", Email = "suzie@example.com", PointsBalance = 10 }
            };
            controller = new UserController(mockDb);
        }
        
        [Fact]
        public void TestGet() {
            var newUser = new UserDTO { Name = "Gregory", Email = "greg@example.com" };
            
            var user = controller.Get("roger@example.com");
            
            Assert.NotNull(user);
            Assert.Equal(1, user.UserId);
            Assert.Equal("Roger", user.Name);
            Assert.Equal("roger@example.com", user.Email);
            Assert.Equal(50m, user.PointsBalance);
        }

        [Fact]
        public void TestCreate() {
            var newUser = new UserDTO { Name = "Gregory", Email = "greg@example.com" };
            
            controller.Create(newUser);
            
            var createdUser = mockDb.SingleOrDefault(u => u.Email == "greg@example.com");
            Assert.NotNull(createdUser);
            Assert.Equal("Gregory", createdUser.Name);
            Assert.Equal("greg@example.com", createdUser.Email);
            Assert.Equal(0m, createdUser.PointsBalance);
        }
        
        [Fact]
        public void TestTransferSucceeds() {
            var result = controller.TransferPoints(1, 2, 20);
            Assert.True(result);
            Assert.Equal(30, mockDb.Single(u => u.UserId == 1).PointsBalance);
            Assert.Equal(90, mockDb.Single(u => u.UserId == 2).PointsBalance);
        }
        
        [Fact]
        public void TestTransferInvalidUserIds() {
            var result = controller.TransferPoints(99, 100, 20);
            Assert.False(result);
        }

        [Fact]
        public void TestTransferInsufficientBalance() {
            var result = controller.TransferPoints(1, 2, 100);
            Assert.False(result);
            Assert.Equal(50, mockDb.Single(u => u.UserId == 1).PointsBalance);
            Assert.Equal(70, mockDb.Single(u => u.UserId == 2).PointsBalance);
        }

    }
}