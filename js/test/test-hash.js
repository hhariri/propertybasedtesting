const hash = require('../src/hash.js');
const assert = require('chai').assert;
const jsc = require('jsverify');

describe('hashPassword', function() {
  
  it('should return 0 for an empty password', function() {
    assert.equal(0, hash.hashPassword(''));
  });
  
  it('should throw an error if passed an invalid parameter', function() {
    assert.throws(() => hash.hashPassword(undefined), TypeError)
  });
  
  it('should hash the password properly', function() {
    assert.equal(1216985755, hash.hashPassword('password'));
    assert.equal(-1569883018, hash.hashPassword('P@55w0rd1'));
    assert.equal(-1424436592, hash.hashPassword('abc123'));
    assert.equal(113631, hash.hashPassword('sam'));
    assert.equal(53026, hash.hashPassword('5@m'));
  });

});