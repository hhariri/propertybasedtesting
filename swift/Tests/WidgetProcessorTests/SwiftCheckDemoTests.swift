//
//  SwiftCheckDemoTests.swift
//  SwiftCheckDemoTests
//
//  Created by Sam Ritchie on 2/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import XCTest
import SwiftCheck
@testable import WidgetProcessor

enum WidgetOperation: Arbitrary, CustomDebugStringConvertible {
    case processWidget(Widget)
    case swizzleDoodad(Doodad)
    case donkifyThingamajig(Thingamajig)
    
    static var arbitrary: Gen<WidgetOperation> {
        let process = Widget.arbitrary.map(WidgetOperation.processWidget)
        let swizzle = Doodad.arbitrary.map(WidgetOperation.swizzleDoodad)
        let donkify = Thingamajig.arbitrary.map(WidgetOperation.donkifyThingamajig)
        
        return Gen.oneOf([process, swizzle, donkify])
    }

    var debugDescription: String {
        switch self {
        case let .processWidget(w): return "processed \(w)\n"
        case let .swizzleDoodad(d): return "swizzled \(d)\n"
        case let .donkifyThingamajig(t): return "donkified \(t)\n"
        }
    }
}

struct WidgetSession: Arbitrary, CustomDebugStringConvertible {
    let processor: WidgetProcessor
    let operations: [WidgetOperation]
    
    init(operations: [WidgetOperation]) {
        self.processor = WidgetProcessor()
        self.operations = operations
        for op in operations {
            switch op {
            case let .processWidget(w): processor.processWidget(w)
            case let .swizzleDoodad(d): processor.swizzleDoodad(d)
            case let .donkifyThingamajig(t): processor.donkifyThingamajig(t)
            }
        }
    }
    
    static var arbitrary: Gen<WidgetSession> {
        return [WidgetOperation].arbitrary.map { ops in
            return WidgetSession(operations: ops)
        }
    }
    
    static func shrink(_ s: WidgetSession) -> [WidgetSession] {
        return [WidgetOperation].shrink(s.operations).map { WidgetSession(operations: $0) }
    }
    
    var debugDescription: String {
        return "WidgetProcessor(sum: \(processor.sum), count: \(processor.count)) completed:\n"
            + operations.reduce("") { s, o in s + o.debugDescription }
    }
}

class SwiftCheckDemoTests: XCTestCase {
    
    func testWidgetProcessor() {
        property("Processor sum should include all widgets processed") <- forAll { (s: WidgetSession) in
            let widgetTotal = s.operations.reduce(0) { sum, op in
                if case let .processWidget(w) = op { return sum + w.value }
                else { return sum }
            }
            return widgetTotal == s.processor.sum
        }

    }
    
}
