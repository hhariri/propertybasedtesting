//
//  WidgetArbitraries.swift
//  SwiftCheckDemo
//
//  Created by Sam Ritchie on 2/05/2016.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import SwiftCheck
@testable import WidgetProcessor

extension WidgetStatus: Arbitrary {
    public static var arbitrary: Gen<WidgetStatus> {
        return Gen<WidgetStatus>.fromElementsOf([.good, .bad, .ugly])
    }
}

extension Widget: Arbitrary {
    public static var arbitrary: Gen<Widget> {
        return Int.arbitrary.flatMap { i in
            WidgetStatus.arbitrary.map { s in
                Widget(value: i, status: s)
            }
        }
    }
}

extension Doodad: Arbitrary {
    public static var arbitrary: Gen<Doodad> {
        return Gen.oneOf([String.arbitrary, Gen.pure("")]).flatMap { s in
            Bool.arbitrary.map { b in
                Doodad(isTransmogrified: b, wimpleGlumpf: s)
            }
        }
    }
}

extension Thingamajig: Arbitrary {
    public static var arbitrary: Gen<Thingamajig> {
        return String.arbitrary.map(Thingamajig.init)
    }
}
