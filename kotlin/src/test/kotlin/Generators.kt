package au.com.codesplice.portablesupernaturalanimals

import io.kotlintest.properties.Gen

val types = listOf(
  "Bulbasaur",
  "Ivysaur",
  "Venusaur",
  "Charmander",
  "Charmeleon",
  "Charizard",
  "Squirtle",
  "Wartortle",
  "Blastoise",
  "Caterpie",
  "Metapod",
  "Butterfree",
  "Weedle",
  "Kakuna",
  "Beedrill",
  "Pidgey",
  "Pidgeotto",
  "Pidgeot",
  "Rattata",
  "Raticate"
)

val itemNames = listOf(
  "Spherical monster collection device",
  "Monster egg",
  "Potion",
  "Super potion",
  "Ultra mega mega potion"
)

class MonsterGenerator : Gen<Monster> {
  override fun generate(): Monster = Monster(Gen.oneOf(types).generate(), 
  Gen.string().generate(), 
  Gen.string().generate(), 
  Gen.choose(0, 200).generate(),
  Gen.choose(0, 200).generate())
}

class DBContextGenerator : Gen<DBContext> {
  override fun generate(): DBContext = DBContext(mutableListOf(), mutableListOf(), Gen.choose(0, 2000).generate()) 
}

class StoreItemGenerator : Gen<StoreItem> {
  override fun generate(): StoreItem = StoreItem(Gen.oneOf(itemNames).generate(), Gen.choose(0, 1000).generate()) 
}

class FriendGenerator : Gen<Friend> {
  override fun generate(): Friend = Friend(Gen.string().generate(), Gen.string().generate())
}

class GymGenerator : Gen<Gym> {
  override fun generate(): Gym = Gym(Gen.oneOf(listOf("Chastity", "Frugality", "Sobriety")).generate(), mutableListOf())
}