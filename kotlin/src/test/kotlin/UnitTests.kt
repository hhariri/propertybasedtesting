package au.com.codesplice.portablesupernaturalanimals

import kotlin.test.*
import org.junit.Test
import io.kotlintest.specs.*
import io.kotlintest.properties.Gen
import com.github.salomonbrys.kotson.*
import com.google.gson.*

class MonsterTests {
    @Test fun toJson() {
      val monster = Monster("Bulbasaur", "6abfe018-014d-4772-aac5-a73786e95374", "That flowery thing", 10, 20)
      val json = monster.toJson()
      assertEquals("Bulbasaur", json["type"].string)
      assertEquals("6abfe018-014d-4772-aac5-a73786e95374", json["id"].string)
      assertEquals("That flowery thing", json["name"].string)
      assertEquals(10, json["hp"].int)
      assertEquals(20, json["cp"].int)
    }

    @Test fun fromJson() {
      val json = jsonObject(
        "type" to "Butterfree",
        "id" to "40b88004-bda3-4a76-a587-d07dfe99061c",
        "name" to "I can’t believe it’s not",
        "hp" to 12,
        "cp" to 23)
      val monster = Monster(json)
      assertEquals("Butterfree", monster.type)
      assertEquals("40b88004-bda3-4a76-a587-d07dfe99061c", monster.id)
      assertEquals("I can’t believe it’s not", monster.name)
      assertEquals(12, monster.hp)
    }
}

class DBContextTests {

    @Test fun addFriend() {
      val ctx = DBContext()
    }
    @Test fun insert() {
      val ctx = DBContext()
      val new = Monster("Bulbasaur", "6abfe018-014d-4772-aac5-a73786e95374", "That flowery thing", 10, 10)
      ctx.collect(new)
      assertTrue(ctx.monsters.contains(new))
    }

    @Test fun getExists() {
      val ctx = DBContext(mutableListOf(
        Monster("Squirtle", "dcfd4e51-02e3-4ba4-8998-9f208214577f", "The wet one", 10, 5),
        Monster("Charmander", "1d4a4969-f28d-439f-b7c0-a70c7958fa50", "The dragony one", 12, 8)
      ))
      val monster = ctx.get("dcfd4e51-02e3-4ba4-8998-9f208214577f")
      assertNotNull(monster)
    }

    @Test fun getDoesntExist() {
      val ctx = DBContext(mutableListOf(
        Monster("Squirtle", "dcfd4e51-02e3-4ba4-8998-9f208214577f", "The wet one", 10, 5),
        Monster("Charmander", "1d4a4969-f28d-439f-b7c0-a70c7958fa50", "The dragony one", 12, 8)
      ))
      val monster = ctx.get("5685753c-10f6-4d8b-8a69-20c904e79adf")
      assertNull(monster)
    }
}


class PropertyBasedTests : StringSpec() {
  init {
    "Roundtrip to Json returns the correct result" {
      forAll(MonsterGenerator(), { monster: Monster -> 
        monster == Monster(monster.toJson())
      })
    }

    "addFriend is idempotent" {
      forAll(DBContextGenerator(), FriendGenerator(), { ctx: DBContext, new: Friend ->
        ctx.addFriend(new)
        val copy = ctx.copy()
        ctx.addFriend(new)
        copy == ctx
      })
    }

    "isFriend returns true after adding a friend" {
      forAll(DBContextGenerator(), FriendGenerator(), { ctx: DBContext, new: Friend ->
        ctx.addFriend(new)
        ctx.isFriend(new.email)
      })
    } 

    "Moving to a gym doesn’t change the total" {
      forAll(DBContextGenerator(), GymGenerator(), MonsterGenerator(), { ctx: DBContext, gym: Gym, monster: Monster ->
        ctx.collect(monster)
        val initialCount = ctx.monsters.count() + gym.monsters.count()
        ctx.moveToGym(gym, monster.id)
        initialCount == ctx.monsters.count() + gym.monsters.count()
      })
    }
  }
}
