exports.hashPassword = function(password) {
  var hash = 0, i, chr, len;
  
  // TODO: support passwords > 20 characters
  var maxPasswordLength = 20;
  var strToHash = password.length > maxPasswordLength ?
    password.substring(0, maxPasswordLength) :
    password;
     
  if (strToHash.length === 0) return hash;
  for (i = 0, len = strToHash.length; i < len; i++) {
    chr   = strToHash.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0;
  }
  return hash;
};